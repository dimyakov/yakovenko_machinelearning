**Источник:**
 https://archive.ics.uci.edu/ml/datasets/Combined+Cycle+Power+Plant

**Описание:**
Набор данных представляет собой показания датчиков электростанции комбинированного цикла. За 6 лет работы электростанции (2006-2011) было собрано 9568 объектов.

Электростанция с комбинированным циклом (ПГУ) состоит из газовых турбин (ГТ), паровых турбин (ПТ) и парогенераторов-утилизаторов. В ПГУ электроэнергия вырабатывается газовыми и паровыми турбинами, которые объединены в один цикл и передаются от одной турбины к другой. В то время как Вакуум формируется и воздействует на Паровую Турбину, другие три окружающих переменных влияют на производительность ГТ.

Данные в исходном документе перемешаны 5 раз (по листам), поэтому будет использоваться только один лист .xlsx файла.

**Признаки:**
Характеристики состоят из среднечасовых параметров окружающей среды: температуры (AT), атмосферного давления (AP), относительной влажности (RH) и вакуума на выхлопе (V). Целевым признаком является чистый почасовой выход электроэнергии (PE) для данной электростанции.

**Статьи:**
Pınar Tüfekci, Prediction of full load electrical power output of a base load operated combined cycle power plant using machine learning methods, International Journal of Electrical Power & Energy Systems, Volume 60, September 2014, Pages 126-140, ISSN 0142-0615, http://dx.doi.org/10.1016/j.ijepes.2014.02.027.
(http://www.sciencedirect.com/science/article/pii/S0142061514000908)

Heysem Kaya, Pınar Tüfekci , Sadık Fikret Gürgen: Local and Global Learning Methods for Predicting Power of a Combined Gas & Steam Turbine, Proceedings of the International Conference on Emerging Trends in Computer and Electronics Engineering ICETCEE 2012, pp. 13-18 (Mar. 2012, Dubai)

